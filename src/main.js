import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import App from './App'
import AdminComponent from './components/admin/Admin.component'
import TeamsComponent from './components/admin/teams/Teams.component'
import TeamsIndexComponent from './components/admin/teams/TeamsIndex.component'
import TeamsNewComponent from './components/admin/teams/TeamsNew.component'

require('./vendor/init.js');

Vue.use(VueRouter)
Vue.use(VueResource)

var router = new VueRouter().map({
  '/': {
    component: AdminComponent,
    subRoutes: {
      '/teams': {
        component: TeamsComponent,
        subRoutes: {
          '/': { component: TeamsIndexComponent },
          '/new': { component: TeamsNewComponent }
        }
      }
    }
  }
});

router.start(App, 'app')